# NOTE: Static Sites often want to optimize for fast build and deploy
#       pipeline times, so changes are published as quickly as possible.
#       Therefore, this default config includes optional variables, settings,
#       and caching, which help minimize job run times. For example,
#       disabling support for git LFS and submodules. There are also retry
#       and reliability settings which help prevent false build failures
#       due to occasional infrastructure availability problems. These are
#       all documented inline below, and can be changed or removed as
#       necessary, depending on the requirements for your repo or project.

###################################
#
# GENERAL/DEFAULT CONFIG:
#
###################################

stages:
  - prepare
  - build
  - deploy
  - accessibility


default:
  image: ruby:2.6.6
  interruptible: true # All jobs are interruptible by default
  # The following 'retry' configuration settings may help avoid false build failures
  #  during brief problems with CI/CD infrastructure availability
  retry:
    max: 2 # This is confusing but this means "3 runs at max".
    when:
      - unknown_failure
      - api_failure
      - runner_system_failure
      - job_execution_timeout
      - stuck_or_timeout_failure

variables:
  # NO_CONTRACTS speeds up middleman builds
  NO_CONTRACTS: 'true'
  a11y_urls: "https://<username>.gitlab.io/<project-name>"

include:
  template: "Verify/Accessibility.gitlab-ci.yml"

  # NOTE: The following PERFORMANCE and RELIABILITY variables are optional, but may
  #       improve performance of larger repositories, or improve reliability during
  #       brief problems with CI/CD infrastructure availability

  ### PERFORMANCE ###
  # GIT_* variables to speed up repo cloning/fetching
  GIT_DEPTH: 10
  # Disabling LFS and submodules will speed up jobs, because runners don't have to perform
  # the submodule steps during repo clone/fetch. These settings can be deleted if you are using
  # LFS or submodules.
  GIT_LFS_SKIP_SMUDGE: 1
  GIT_SUBMODULE_STRATEGY: none

  ### RELIABILITY ###
  # Reduce potential of flaky builds via https://docs.gitlab.com/ee/ci/yaml/#job-stages-attempts variables
  GET_SOURCES_ATTEMPTS: 3
  ARTIFACT_DOWNLOAD_ATTEMPTS: 3
  RESTORE_CACHE_ATTEMPTS: 3
  EXECUTOR_JOB_SECTION_ATTEMPTS: 3

### COMMON JOBS REUSED VIA `extends`:

.ruby-cache:
  cache:
    key: "ruby-2.6"
    policy: pull
    paths:
      - vendor

.bundle-install:
  extends: .ruby-cache
  before_script:
    - apt-get update -qq && apt-get install -y -qq nodejs
    - gem install bundler --no-document
    - bundle config set path 'vendor'
    - bundle install --quiet --jobs 4

###################################
#
# PREPARE STAGE
#
###################################

ruby-push-cache:
  extends: .bundle-install
  stage: prepare
  rules:
    # Only run this job from pipelines for the default branch
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  cache:
    # Only push the cache from this job, to save time on all other jobs.
    policy: pull-push
  script:
    - echo "Pushing updated ruby cache..."

###################################
#
# BUILD STAGE
#
###################################

build:
  extends: .bundle-install
  stage: build
  rules:
    # NOTE: The 'build' job only runs for Merge Requests and tags, and does not
    #       do a deploy. If you want to deploy from Merge Request branches, you
    #       can use Review Apps: https://docs.gitlab.com/ee/ci/review_apps
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
  script:
    - bundle exec middleman build --bail

###################################
#
# DEPLOY STAGE
#
###################################

# NOTE: In order to keep the pipeline as fast as possible for the default branch
#       we combine both the build and deploy to GitLab pages in a single job.
pages:
  extends: .bundle-install
  stage: deploy
  rules:
    # Only run the deploy on the default branch. If you want to deploy from Merge Request branches,
    # you can use Review Apps: https://docs.gitlab.com/ee/ci/review_apps
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  needs: [] # Prevents job from being blocked by jobs in previous stages.
  script:
    - bundle exec middleman build --bail
  artifacts:
    paths:
      - public
